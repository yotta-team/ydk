import { Router } from 'express';

export interface IServerApp {
    onInstall(): Promise<void>;

    onUninstall(): Promise<void>;

    onStart(): Promise<void>;

    onStop(): Promise<void>;

    getEndpoint(): Promise<Router>;

    getRouter(): Promise<Router>;
}