import { Router } from 'express';

import { IInstance } from '../models/instance.model';

export interface IServerInstance {
    start(): Promise<void>;

    stop(): Promise<void>;

    getEndpoint(): Promise<Router>;

    getRouter(): Promise<Router>;
}