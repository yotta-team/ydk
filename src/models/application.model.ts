import { connection } from '../services/mongoose.service';
import { Document, Model, Schema } from 'mongoose';
import { RepositorySchema, IRepository } from './repository.model';

export interface IApplication extends Document {
    _id: string,
    name: string;
    image: string;
    description: string;
    repository: IRepository;
};

export const ApplicationSchema = new Schema({
    name: String,
    image: String,
    description: String,
    repository: { type: RepositorySchema }
});

export const Application = connection.model<IApplication>('Application', ApplicationSchema);