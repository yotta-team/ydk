import { Document, Schema } from 'mongoose';

import { IApplication } from './application.model';

export const InstanceAppStatus = {
    Installed: 'Installed',
    Configured: 'Configured', 
    Working: 'Working', 
    Broken: 'Broken', 
    Stopped: 'Stopped',
    Removed: 'Removed'
};

export interface IInstanceApp extends Document {
    _id: string,
    version: string;
    status: string;
    application: IApplication;
};

export const InstanceAppSchema = new Schema({
    version: String,
    status: { type: String, enum: Object.keys(InstanceAppStatus) },
    application: { type: Schema.Types.ObjectId, ref: 'Application' }
});
