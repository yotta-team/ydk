import { connection } from '../services/mongoose.service';
import { Document, Model, Schema } from 'mongoose';

import { InstanceAppSchema, IInstanceApp } from './instance-app.model';

export const InstanceStatus = {
    Created: 'Created',
    Active: 'Active',
    Inactive: 'Inactive'
};

export const InstanceType = {
    Development: 'Development',
    Local: 'Local',
    External: 'External'
};

export interface IInstance extends Document {
    _id: string,
    port: number;
    host: string;
    protocol: string;
    ip: string;
    folder: string;
    name: string;
    description: string;
    version: string;
    status: string;
    type: string;
    apps: IInstanceApp[];
}

export const InstanceSchema = new Schema({
    port: Number,
    host: String,
    protocol: String,
    ip: String,
    folder: String,
    name: String,
    description: String,
    version: String,
    status: { type: String, enum: Object.keys(InstanceStatus) },
    type: { type: String, enum: Object.keys(InstanceType) },
    apps: [InstanceAppSchema]
});

export const Instance = connection.model<IInstance>('Instance', InstanceSchema);