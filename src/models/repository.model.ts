import { Document, Schema } from 'mongoose';

export interface IRepository extends Document {
    _id: string,
    name: string;
    url: string;
};

export const RepositorySchema = new Schema({
    name: String,
    url: String,
});