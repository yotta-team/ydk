import { connection } from '../services/mongoose.service';
import { Document, Schema, Model } from 'mongoose';

export const UserStatus = {
    Active: 'Active',
    Inactive: 'Inactive'
};

export interface IUser extends Document {
    _id: string,
    email: string;
    password: string;
    name: string;
    lastAccess: Date;
    status: string;
    instances: string[];
}

export const UserSchema = new Schema({
    email: { type: String, index: true },
    password: String,
    name: String,
    lastAccess: Date,
    status: { type: String, enum: Object.keys(UserStatus) },
    instances: [ { type: Schema.Types.ObjectId, ref: 'Instance' } ]
});

export const User = connection.model<IUser>('User', UserSchema);