import * as request from 'request';

export class ApiService {
    constructor(
        private apiUrl: string,
        private token?: string) {
        if (apiUrl.endsWith('/'))
            this.apiUrl = apiUrl.substring(0, apiUrl.length - 1);
    }

    private _authRequestHandler<T>(url: string, method: string, body?: object): Promise<T> {
        if (!this.token) return Promise.reject(new Error('Authentication it\'s required to use the api.'));
        return this._requestHandler<T>(url, method, body,  {
            'authorization': this.token
        });
    }

    private _requestHandler<T>(url: string, method: string, body?: object, headers?: any): Promise<T> {
        if (url.startsWith('/')) url = url.substring(1);
        url = `${this.apiUrl}/${url}`;
        return new Promise((resolve, reject) => {
            const options = {
                url: url,
                body: body,
                json: true,
                method: method,
                headers: headers
            };
            const req = request(options, (err, httpResponse, body) => {
                if(err || httpResponse.statusCode !== 200) reject(err || body);
                else resolve(body as T);
            });
            req.on('error', function(err) {
                reject(err);
            });
        });
    }

    authenticate(key: string): Promise<void> {
        return this._requestHandler<any>(`authenticate?key=${key}`, 'GET')
            .then((res) => this.token = res.token);
    }

    post(url: string, body: object): Promise<any>;
    post<T>(url: string, body: object): Promise<T> {
        return this._authRequestHandler<T>(url, 'POST', body);
    }

    put(url: string, body: object): Promise<any>;
    put<T>(url: string, body: object): Promise<T> {
        return this._authRequestHandler<T>(url, 'PUT', body);
    }

    delete(url: string): Promise<any>;
    delete<T>(url: string): Promise<T> {
        return this._authRequestHandler<T>(url, 'DELETE');
    }

    get(url: string): Promise<any>;
    get<T>(url: string): Promise<T> {
        return this._authRequestHandler<T>(url, 'GET');
    }
}