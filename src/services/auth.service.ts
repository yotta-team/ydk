import * as md5 from 'md5';
import * as path from 'path';
import { Express } from 'express';
import * as request from 'request';
import * as passport from 'passport';
import { Strategy } from 'passport-local';
import * as expressSession from 'express-session';

import { Logger } from './logger.service';
import { IInstance } from '../models/instance.model';
import { User, IUser, UserStatus } from '../models/user.model';
import { Query } from './query.service';

export class AuthService {

    query: Query<IUser>;
    logger:Logger;

    isValidPassword(user: IUser, password: string) {
        return md5(password) == user.password;
    }

    constructor(app: Express, instance: IInstance, logger: Logger) {
        this.logger = logger;
        this.query = new Query<IUser>(User);
        
        app.use(expressSession({ secret: instance.host + instance._id, cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false }));
        app.use(passport.initialize());
        app.use(passport.session());
        
        passport.use(new Strategy(
            {
                usernameField: 'email',
                passwordField: 'password'
            },
            function (email: string, password: string, done: (a: any, b: any, c?: any) => void) {
                if (process.env.IsDev) {
                    this.logger.log('IsDev login mocked!');
                    email = 'michaelpereiradasilva@hotmail.com';
                    password = '123456';
                }

                this.query.get({ email: email })
                    .then(function (user: IUser) {
                        if (!user || !this.isValidPassword(user, password)) {
                            this.logger.log(`User ${email} login failed at ${new Date()}`);
                            return done(null, false, { message: 'Email e/ou senha inválidos.' });
                        }
                        if (user.status !== UserStatus.Active) {
                            this.logger.log(`User ${email} login failed at ${new Date()}`);
                            return done(null, false, { message: 'Este usuário esta inativo.' });
                        }

                        this.logger.log(`User ${email} logged at ${new Date()}`);
                        done(null, user);
                    });
            }));

        passport.serializeUser(function (user: IUser, done) {
            done(null, user._id);
        });

        passport.deserializeUser(function (id, done) {
            this.query.get({ _id: id })
                .then(function (user: IUser) {
                    done(null, user);
                })
                .catch(function (err: any) {
                    done(err, null);
                });
        });
    }
}