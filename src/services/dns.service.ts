import { Instance, IInstance } from '../models/instance.model';

export class DnsService {
    static PREFFIX: string = 'yotta';
    
    private options: any;
    constructor(options?: object) {
        this.options = options || {};
    }

    init(coreUrl: string) {
        return new Promise((resolve, reject) => {
            Instance.find((err: any, instances?: IInstance[]) => {
                if (err) return reject(err);

                for (let key in instances) {
                    this.add(instances[key], coreUrl);
                }

                resolve();
            });
        });
    }

    add(instance: IInstance, coreUrl: string) {
        const server = require(`../../instances/${instance.host}/server.js`);
        server.start(instance, coreUrl);
        this.options[instance.host] = `${instance.protocol}://${instance.ip}:${instance.port}`;

        if (!instance.host.startsWith(DnsService.PREFFIX))
            this.options[`${DnsService.PREFFIX}${instance.host}`] = `${instance.protocol}://${instance.ip}:${instance.port}/yotta`;
    }

    resolve(host: string) {
        return this.options[host];
    }
};