export class ErrorService {
    static default(res, e: Error) {
        console.error(e);
        res.status(500).json({ error: e.message });
    }
    
    static notAuthorized(res) {
        res.status(401);
    }
}