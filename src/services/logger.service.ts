
import * as path from 'path';

import { fsx } from '../utils/fsx.util';

export class Logger {
    logPath: string;

    constructor(logPath: string) {
        this.logPath = logPath;
    }

    private async _appendLine(message: string) {
        return await fsx.appendFile(path.resolve(`${this.logPath}`, `${new Date().getMonth()}.txt`), `${message}\n`);
    }

    async log(...args: any[]): Promise<void[]> {
        const logs = [];
        for (const arg of args) {
            logs.push(await this._appendLine((typeof (arg) === 'object') ? JSON.stringify(arg) : arg));
        }
        return Promise.all(logs);
    }
}