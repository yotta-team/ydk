import * as path from 'path';
import { Transporter, SentMessageInfo, createTransport } from 'nodemailer';

import { fsx } from '../utils/fsx.util';

export class MailSettings {
    from: string;
    service: string;
    auth: {
        user: string;
        pass: string;
    }
}

export class Mailer {
    private _from: string;
    private _transporter: Transporter;
    private _settings: MailSettings;

    constructor(settings: MailSettings) {
        this._settings = settings;
        this._from = settings.from;

        // create reusable transporter object using the default SMTP transport
        this._transporter = createTransport({
            service: settings.service,
            auth: {
                user: settings.auth.user,
                pass: settings.auth.pass
            }
        });
    }

    send(to: string, subject: string, html: string): Promise<void> {
        return new Promise((resolve, reject) => {
            const options = {
                from: this._from,
                to: to,
                subject: subject,
                html: html
            };

            // send mail with defined transport object
            this._transporter.sendMail(options, (error: Error, info: SentMessageInfo) => {
                if (error) reject(error);
                resolve();
            });
        });
    }

    sendFile(to: string, subject: string, file: string, params?: any): Promise<void> {
        const source = path.resolve(`${__dirname}/../emails/${file}`);
        return fsx.readFile(source)
            .then((data: string) => {
                let result = data;
                if (params) {
                    for (let key in params)
                        result = result.replace(new RegExp(`{{${key}}}`, 'g'), params[key]);
                }

                return this.send(to, subject, result);
            });
    }
}