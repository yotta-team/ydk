import * as mongoose from 'mongoose';
import { Settings } from './settings.service';

(<any>mongoose).Promise = Promise;
const connectionString = process.env.MONGODB_CONNECTION || Settings.get('connectionString');
if(!connectionString) 
    throw new Error('It\'s required to set a connectionString to settings');

export const connection: mongoose.Connection = mongoose.createConnection(connectionString, { 
    server: { socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 } }, 
    replset: { socketOptions: { keepAlive: 300000, connectTimeoutMS : 30000 } } 
}); // connect to our database

connection.on('connected', function () {
    console.log('Mongoose default connection open to ');
});

connection.on('error', function (err) {
    console.log('Mongoose default connection error: ' + err);
});

connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});

connection.on('open', function () {
    console.log('Mongoose default connection is open');
});

process.on('SIGINT', function () {
    connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});