
import { Document, Model, Schema } from 'mongoose';

export class Query<T extends Document> {
    private _model: Model<T>;
    private _constraint: object;
    constructor(model: Model<T>, constraint?: object) {
        this._model = model;
        this._constraint = constraint || {};
    }

    get(where?: object): Promise<T> {
        Object.assign(where || {}, this._constraint);
        return new Promise((resolve, reject) => {
            this._model.findOne(where, function (err: any, model?: T) {
                if (err) return reject(err);
                else resolve(model || undefined);
            });
        });
    }

    getAll(where?: any): Promise<T[]> {
        Object.assign(where || {}, this._constraint);
        return new Promise((resolve, reject) => {
            this._model.find(where, function (err: any, models?: T[]) {
                if (err) return reject(err);
                else resolve(models || []);
            });
        });
    }

    select<TResult>(projection: TResult, where?: any): Promise<TResult[]> {
        Object.assign(where || {}, this._constraint);
        return new Promise((resolve, reject) => {
            this._model.find(where, projection, function (err: any, models?: TResult[]) {
                if (err) return reject(err);
                else resolve(models || []);
            });
        });
    }

    selectOne<TResult>(projection: TResult, where?: any): Promise<TResult> {
        Object.assign(where || {}, this._constraint);
        return new Promise((resolve, reject) => {
            this._model.findOne(where, projection, function (err: any, model?: TResult) {
                if (err) return reject(err);
                else resolve(model || undefined);
            });
        });
    }

    max<TResult>(field: string, where?: any): Promise<TResult> {
        Object.assign(where || {}, this._constraint);
        return new Promise((resolve, reject) => {
            this._model.findOne(where).sort(`-${field}`)  // give me the max
                .exec(function (err: any, result?: any) {
                    if (err) return reject(err);
                    else resolve(result ? result[field] : undefined);
                });
        });
    }
}
