import { Response } from 'express';

export class Settings {
    private static data = new Map<string, any>();

    static get(key: string) {
        return Settings.data.get(key);
    }

    static set(key: string, value: any) {
        Settings.data.set(key, value);
    }
    
    static readJSON(object: any) {
        for(const key in object) {
            Settings.set(key, object[key]);
        }
    }
}