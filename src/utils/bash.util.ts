import { exec } from 'child_process';

export class bash {
    static exec(command: string): Promise<void> {
        return new Promise((resolve, reject) => {
            exec(command, function (err, stdout, stderr) {
                if (err) reject(err);
                else resolve();
            });
        });
    }
}