import { Document } from 'mongoose';

export class Commons {

    static removeId(obj: any) {
        for (var key in obj) {
            if (key.startsWith("_"))
                delete obj[key];
            else if (typeof obj[key] == "object")
                Commons.removeId(obj[key]);
        }
    }

    static cloneDoc<T extends Document>(doc: T): T {
        let obj = typeof(doc.toObject) === 'function' ? doc.toObject() : doc;
        Commons.removeId(obj);
        return obj as T;
    }

    static cloneArray<T extends Document>(docs: T[]): T[] {
        const newDocs = docs.map(Commons.cloneDoc);
        return newDocs;
    }

    static urlToFileName(url: string) {
        let parts = url.replace(/(\\|\/\/|\/$)/gi, '/').replace(/(^\/|\/$)/gi, '').split('/');
        let lastPart = parts[parts.length - 1];
        if (lastPart.indexOf('.html') === -1) {
            parts[parts.length - 1] += '/index.html';
        }
        return parts.join('/');
    }

    static stripAccents(text: string) {
        var in_chrs = 'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ',
            out_chrs = 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY',
            chars_rgx = new RegExp('[' + in_chrs + ']', 'g'),
            transl: any = {}, 
            i: number,
            lookup = (m: string) => transl[m] || m;

        for (i = 0; i < in_chrs.length; i++) {
            transl[in_chrs[i]] = out_chrs[i];
        }

        return text.replace(chars_rgx, lookup);
    }

    static urlFrom(text: string) {
        return Commons.stripAccents(text.toLowerCase())
            .replace(/\s/g, '-')
            .replace(/-+/g, '-')
            .replace(/\\/g, '/')
            .replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
    }

    static anchorFrom(text: string) {
        return '#' + Commons.stripAccents(text.toLowerCase())
            .replace(/(\s|\/|\.)/g, '-')
            .replace(/-+/g, '-')
            .replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
    }
}