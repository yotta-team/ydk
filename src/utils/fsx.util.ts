import * as fs from 'fs';
import * as path from 'path';

import { ncp } from 'ncp';

const unzip = require('unzip');

export class fsx {

    static exists(dir_path: string | Buffer): Promise<boolean> {
        return new Promise(function (resolve, reject) {
            fs.exists(dir_path, function (exists) {
                resolve(exists);
            });
        });
    }

    static isEmpty(dir_path: string | Buffer): Promise<boolean> {
        return fsx.readdir(dir_path).then(files => files.length === 0);
    }

    static isdir(entry_path: string | Buffer): Promise<boolean> {
        return new Promise(function (resolve, reject) {
            fs.lstat(entry_path, function (err, stats) {
                if (err) reject(err);
                else resolve(stats.isDirectory());
            });
        });
    }

    static extractZip(source: string, target: string) {
        return new Promise((resolve, reject) => {
            fs.createReadStream(source)
                .pipe(unzip.Extract({ path: target }))
                .on('close', function (err: string) {
                    if (err) reject(err);
                    else resolve();
                });
        });
    }

    static readdir(dir_path: string | Buffer): Promise<string[]> {
        return new Promise(function (resolve, reject) {
            fs.readdir(dir_path, function (err, files) {
                if (err) reject(err);
                else resolve(files);
            });
        });
    }

    static mkdir(dir_path: string | Buffer): Promise<void> {
        return new Promise(function (resolve, reject) {
            fs.mkdir(dir_path, function (err) {
                if (err) reject(err);
                else resolve();
            });
        });
    }

    static rmdir(dir_path: string | Buffer): Promise<void> {
        return new Promise(function (resolve, reject) {
            fs.rmdir(dir_path, function (err) {
                if (err) reject(err);
                else resolve();
            });
        });
    }

    static removeFile(entry_path: string | Buffer): Promise<void> {
        return new Promise(function (resolve, reject) {
            fs.unlink(entry_path, function (err) {
                if (err) reject(err);
                else resolve();
            });
        });
    }

    static writeFile(entry_path: string, content: any, options?: string | any): Promise<void> {
        return new Promise(function (resolve, reject) {
            fs.writeFile(entry_path, content, options, function (err) {
                if (err) reject(err);
                else resolve();
            });
        });
    }

    static readFile(entry_path: string, contentType?: string): Promise<string> {
        return new Promise(function (resolve, reject) {
            fs.readFile(entry_path, contentType || 'utf8', function (err, result) {
                if (err) reject(err);
                else resolve(result);
            });
        });
    }

    static appendFile(entry_path: string, content: any): Promise<void> {
        return new Promise(function (resolve, reject) {
            fsx.exists(entry_path)
                .then((exists) => {
                    if (exists)
                        return fsx.appendFile(entry_path, content);
                    else
                        return fsx.writeFile(entry_path, content);
                });

        });
    }

    static ncp(dir_path: string, target_path: string): Promise<void> {
        return new Promise(function (resolve, reject) {
            ncp(dir_path, target_path, function (err) {
                if (err) return reject(err);
                else resolve();
            });
        });
    }

    static copy(source: string | Buffer, target: string): Promise<any> {
        return new Promise(function (resolve, reject) {
            const dirname = path.dirname(target);
            return fsx.exists(dirname)
                .then((exists) => {
                    if (!exists) return fsx.mkdir(dirname);
                })
                .then(() => {
                    const rd = fs.createReadStream(source);
                    rd.on('error', rejectCleanup);
                    const wr = fs.createWriteStream(target);
                    wr.on('error', rejectCleanup);
                    function rejectCleanup(err: Error) {
                        rd.destroy();
                        wr.end();
                        reject(err);
                    }
                    wr.on('finish', resolve);
                    rd.pipe(wr);
                })
                .catch(reject);
        });
    }

    static move(entry_path: string | Buffer, target_path: string): Promise<void> {
        return fsx.copy(entry_path, target_path)
            .then(() => fs.unlinkSync(entry_path));
    }

    static rename(entry_path: string, target_path: string): Promise<void> {
        return new Promise(function (resolve, reject) {
            fs.rename(entry_path, target_path, function (err) {
                if (err) reject(err);
                else resolve();
            });
        });
    }

    /**
     * Remove directory recursively
     * @param {string} dir_path
     * @see http://stackoverflow.com/a/42505874/3027390
     */
    static rimraf(dir_path: string): Promise<void> {
        function remove(dir_path: string) {
            return fsx.exists(dir_path)
                .then((exists) => exists ? fsx.readdir(dir_path) : [])
                .then(files => {
                    const promises: Promise<string>[] = [];
                    files.forEach(function (entry) {
                        const entry_path = path.join(dir_path, entry);
                        promises.push(fsx.isdir(entry_path)
                            .then(isdir => isdir ? remove(entry_path) : fsx.removeFile(entry_path).then(() => '')));
                    });

                    return Promise.all(promises);
                })
                .then(dirs => Promise.all(dirs.filter(d => d.trim().length > 0).map(fsx.rmdir)))
                .then(() => dir_path);
        }

        const hasStar = dir_path.endsWith('*');
        dir_path = hasStar ? dir_path.substr(0, dir_path.length - 1) : dir_path;

        return remove(dir_path)
            .then(() => {
                if (!hasStar)
                    return fsx.rmdir(dir_path);
            });
    }
}