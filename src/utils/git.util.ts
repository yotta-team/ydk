import { bash } from './bash.util';
import { IRepository } from '../models/repository.model';

export class git {
    static async clone(repoName: string, path: string): Promise<void> {
        await bash.exec(`git clone ${repoName} ${path}`);
    }

    static async cloneRepository(repository: IRepository, path: string): Promise<void> {
        await this.clone(`${repository.url}`, path);
    }

    static async checkout(path: string, files: string = '.') {
        await bash.exec(`cd ${path} && git checkout -- ${files}`);
    }

    static async commit(message: string, path: string, files: string = '.') {
        await bash.exec(`cd ${path} && git add ${files}`);
        await bash.exec(`cd ${path} && git commit -m "${message}"`);
    }

    static async push(path: string, remote: string, branch: string = 'master') {
        await bash.exec(`cd ${path} && git push ${remote} ${branch}`);
    }

    static async pull(path: string, remote: string, branch: string = 'master') {
        await bash.exec(`cd ${path} && git pull ${remote} ${branch}`);
    }
}