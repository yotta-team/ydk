export class Listener {
    private _subscribers: Array<any>;
    constructor() {
        this._subscribers = [];
    }

    subscribe(callback: (params?: any[]) => any) {
        this._subscribers.push(callback);
    }

    notifyAll(params?: any[]) {
        if (this._subscribers.length) {
            for (var i = 0; i < this._subscribers.length; i++) {
                this._subscribers[i].apply(this, params || []);
            }
        }
    }
}