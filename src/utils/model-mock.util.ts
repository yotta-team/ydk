import { Document, Model, Schema } from 'mongoose';


export class ModelMock<T extends Document> {
    
    constructor(private model: Model<T>) { }

    private _createOrUpdateOneItem(keys: string[], data: object) {
        const where: any = {};
        keys.forEach(key => where[key] = (<any>data)[key]);
        
        return this.createOrUpdateOne(where, data);
    }

    createOrUpdate(keys: string | string[], data: object | object[]): Promise<T[]> {
        if(!keys) throw new Error(`It's required to pass a key to createOrUpdate method`);
        if(!Array.isArray(keys)) keys = [ keys ];

        if(Array.isArray(data)) 
            return Promise.all(data.map(d => this._createOrUpdateOneItem(<string[]>keys, d)));
        else 
            return this._createOrUpdateOneItem(keys, data).then((model) => [ model ]);
    }

    createOrUpdateOne(where: object, data: object): Promise<T> {
        return new Promise((resolve, reject) => {
            this.model.findOneAndUpdate(where, data, (err: Error, model: T) => {
                if (err) return reject(err);
                if (!model) {
                    model = new this.model(data);
                }

                model.save((err: Error) => {
                    if (err) return reject(err);
                    resolve(model);
                });
            });
        });
    }
};