import { bash } from './bash.util';
import { IRepository } from '../models/repository.model';

export class npm {
    static install(name: string, save: boolean = false): Promise<void> {
        let args = save ? '--save' : '';
        return bash.exec(`npm install ${name} ${args}`);
    }

    static installRepository(repository: IRepository, save: boolean = false): Promise<void> {
        return this.install(`${repository.url}`, save);
    }

    static uninstall(name: string, save: boolean = false): Promise<void> {
        let args = save ? '--save' : '';
        return bash.exec(`npm uninstall ${name} ${args}`);
    }

    static uninstallRepository(repository: IRepository, save: boolean = false): Promise<void> {
        return this.uninstall(`${repository.name}`, save);
    }

    static update(name: string = '', save: boolean = false): Promise<void> {
        let args = save ? '--save' : '';
        return bash.exec(`npm update ${name} ${args}`);
    }

    static run(scriptName: string): Promise<void> {
        return bash.exec(`npm run ${scriptName}`);
    }
}