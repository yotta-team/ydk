import { Listener } from './listener.util';

export class Observable {
    private _events: Map<string, Listener>;
    constructor() {
        this._events = new Map<string, Listener>();
    }

    on(eventName: string, callback: (params?:any[]) => any) {
        if (!this._events.has(eventName))
            this._events.set(eventName, new Listener());
        
        this._events.get(eventName).subscribe(callback);
    }

    trigger(eventName: string, params?: any[]) {
        if (this._events.has(eventName)) {
            this._events.get(eventName).notifyAll(params || []);
        }
    }
}

