import { mockDb } from './main-test';
import { expect, use, assert } from 'chai';
import { stub, SinonStub } from 'sinon';

import { ApiService } from '../src/services/api.service';
import { ITemplate } from '../src/models/template.model';
import { IInstance } from '../src/models/instance.model';

describe('Site service tests', () => {

    let service: ApiService;

    before(() => {
        service = new ApiService('http://localhost:5000');
    });

    /**tests need to start api before run */

    
    /*it('Get a method with success', () => {
        const token = '5985e04f2d31be3974e172d5';
        return service.authenticate(token)
            .then(() => service.get('v1/templates'))
            .then((templates) => {
                expect(templates).to.be.not.equals(null).and.not.equals(undefined);
                expect(templates.length).to.be.equals(mockDb.templates.length);
            });
    });

    it('Get a method with error', (done) => {
        const token = '5985e04f2d31be3974e172d5';
        service.authenticate(token)
            .then(() => service.get('v1/templates1'))
            .catch(() => done());
    });
    
    it('Get a two methods with success', () => {
        const token = '5985e04f2d31be3974e172d5';
        return service.authenticate(token)
            .then(() => service.get('v1/templates'))
            .then((templates) => {
                expect(templates).to.be.not.equals(null).and.not.equals(undefined);
                expect(templates.length).to.be.equals(mockDb.templates.length);
            })
            .then(() => service.get('v1/templates'))
            .then((templates) => {
                expect(templates).to.be.not.equals(null).and.not.equals(undefined);
                expect(templates.length).to.be.equals(mockDb.templates.length);
            });
    });

    it('Post a method with success', () => {
        const token = '5985e04f2d31be3974e172d5';
        const data = { host: 'localhost', name: 'Editor test', email: 'michaelpereiradasilva@hotmail.com' };
        return service.authenticate(token)
            .then(() => service.post('v1/instances', data))
            .then((instance) => {
                expect(instance).to.be.not.equals(null).and.not.equals(undefined);
            });
    });*/

});