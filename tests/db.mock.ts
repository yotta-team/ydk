import * as md5 from 'md5';

import { Template, ITemplate } from '../src/models/template.model';
import { Site, ISite } from '../src/models/site.model';
import { User, IUser } from '../src/models/user.model';
import { Instance, IInstance } from '../src/models/instance.model';
import { ModelMock } from '../src/utils/model-mock.util';
import { connection } from '../src/services/mongoose.service';


export class DbMock {
    static create() {
        let db: any = {};
        const templatesMock = require('./mocks/templates.mock.json') as ITemplate[];
        const usersMock = require('./mocks/users.mock.json') as IUser[];
        const instancesMock = require('./mocks/instances.mock.json') as IInstance[];
        const sitesMock = require('./mocks/sites.mock.json') as ISite[];

        return new ModelMock(Template).createOrUpdate("folder", templatesMock)
            .then((templates) => {
                db.templates = templates;
                return new ModelMock(Instance)
            })
            .then(model => model.createOrUpdate("port", instancesMock))
            .then((instances) => {
                db.instances = instances;
                return new ModelMock(User)
            })
            .then(model => {
                usersMock.forEach(user => {
                    user.password = md5(user.password);
                });
                return model.createOrUpdate("email", usersMock);
            })
            .then((users) => {
                db.users = users;
                return new ModelMock(Site)
            })
            .then(model => {
                sitesMock.forEach(site => {
                    site._instanceId = db.instances[parseInt(site._instanceId)]._id;
                });
                return model.createOrUpdate("_instanceId", sitesMock);
            })
            .then((sites) => {
                db.sites = sites;
                return db;
            })
    }

    static delete() {
        connection.dropDatabase();
    }
}