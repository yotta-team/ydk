import 'mocha';
import * as path from 'path';

import { Settings } from '../src/services/settings.service';

export let mockDb: any = {};
Settings.readJSON({
    "root": path.resolve(__dirname, '../'),
    "admin": {
        "email": "michaelpereiradasilva@hotmail.com"
    },
    "connectionString": "mongodb://localhost::27017/yes-test",
    "emails":  {
        "register": "new-client.html",
        "firstAccess": "first-access-link.html"
    },
    "mailSettings": {
        "from": "'Yotta' <mike.mps27@gmail.com>",
        "service": "hotmail",
        "auth": {
            "user": "michaelpereiradasilva@hotmail.com",
            "pass": "mike270609leslie"
        }
    }
});

import { DbMock } from './db.mock';

before((done) => {
    DbMock.create()
        .then((db) => {
            console.info(db.sites.length);
            mockDb = db;
            done();
        });
});

after(() => {
    DbMock.delete();
});