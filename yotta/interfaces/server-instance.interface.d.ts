/// <reference types="express" />
import { Router } from 'express';
export interface IServerInstance {
    start(): Promise<void>;
    stop(): Promise<void>;
    getEndpoint(): Promise<Router>;
    getRouter(): Promise<Router>;
}
