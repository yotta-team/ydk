/// <reference types="mongoose" />
import { Document, Model, Schema } from 'mongoose';
import { IRepository } from './repository.model';
export interface IApplication extends Document {
    _id: string;
    name: string;
    image: string;
    description: string;
    repository: IRepository;
}
export declare const ApplicationSchema: Schema;
export declare const Application: Model<IApplication>;
