"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_service_1 = require("../services/mongoose.service");
var mongoose_1 = require("mongoose");
var repository_model_1 = require("./repository.model");
;
exports.ApplicationSchema = new mongoose_1.Schema({
    name: String,
    image: String,
    description: String,
    repository: { type: repository_model_1.RepositorySchema }
});
exports.Application = mongoose_service_1.connection.model('Application', exports.ApplicationSchema);
//# sourceMappingURL=application.model.js.map