/// <reference types="mongoose" />
import { Document, Schema } from 'mongoose';
import { IApplication } from './application.model';
export declare const InstanceAppStatus: {
    Installed: string;
    Configured: string;
    Working: string;
    Broken: string;
    Stopped: string;
    Removed: string;
};
export interface IInstanceApp extends Document {
    _id: string;
    version: string;
    status: string;
    application: IApplication;
}
export declare const InstanceAppSchema: Schema;
