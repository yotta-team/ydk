"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_1 = require("mongoose");
exports.InstanceAppStatus = {
    Installed: 'Installed',
    Configured: 'Configured',
    Working: 'Working',
    Broken: 'Broken',
    Stopped: 'Stopped',
    Removed: 'Removed'
};
;
exports.InstanceAppSchema = new mongoose_1.Schema({
    version: String,
    status: { type: String, enum: Object.keys(exports.InstanceAppStatus) },
    application: { type: mongoose_1.Schema.Types.ObjectId, ref: 'Application' }
});
//# sourceMappingURL=instance-app.model.js.map