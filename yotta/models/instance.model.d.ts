/// <reference types="mongoose" />
import { Document, Model, Schema } from 'mongoose';
import { IInstanceApp } from './instance-app.model';
export declare const InstanceStatus: {
    Created: string;
    Active: string;
    Inactive: string;
};
export declare const InstanceType: {
    Development: string;
    Local: string;
    External: string;
};
export interface IInstance extends Document {
    _id: string;
    port: number;
    host: string;
    protocol: string;
    ip: string;
    folder: string;
    name: string;
    description: string;
    version: string;
    status: string;
    type: string;
    apps: IInstanceApp[];
}
export declare const InstanceSchema: Schema;
export declare const Instance: Model<IInstance>;
