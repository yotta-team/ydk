"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_service_1 = require("../services/mongoose.service");
var mongoose_1 = require("mongoose");
var instance_app_model_1 = require("./instance-app.model");
exports.InstanceStatus = {
    Created: 'Created',
    Active: 'Active',
    Inactive: 'Inactive'
};
exports.InstanceType = {
    Development: 'Development',
    Local: 'Local',
    External: 'External'
};
exports.InstanceSchema = new mongoose_1.Schema({
    port: Number,
    host: String,
    protocol: String,
    ip: String,
    folder: String,
    name: String,
    description: String,
    version: String,
    status: { type: String, enum: Object.keys(exports.InstanceStatus) },
    type: { type: String, enum: Object.keys(exports.InstanceType) },
    apps: [instance_app_model_1.InstanceAppSchema]
});
exports.Instance = mongoose_service_1.connection.model('Instance', exports.InstanceSchema);
//# sourceMappingURL=instance.model.js.map