/// <reference types="mongoose" />
import { Document, Schema } from 'mongoose';
export interface IRepository extends Document {
    _id: string;
    name: string;
    url: string;
}
export declare const RepositorySchema: Schema;
