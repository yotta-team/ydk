/// <reference types="mongoose" />
import { Document, Schema, Model } from 'mongoose';
export declare const UserStatus: {
    Active: string;
    Inactive: string;
};
export interface IUser extends Document {
    _id: string;
    email: string;
    password: string;
    name: string;
    lastAccess: Date;
    status: string;
    instances: string[];
}
export declare const UserSchema: Schema;
export declare const User: Model<IUser>;
