"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose_service_1 = require("../services/mongoose.service");
var mongoose_1 = require("mongoose");
exports.UserStatus = {
    Active: 'Active',
    Inactive: 'Inactive'
};
exports.UserSchema = new mongoose_1.Schema({
    email: { type: String, index: true },
    password: String,
    name: String,
    lastAccess: Date,
    status: { type: String, enum: Object.keys(exports.UserStatus) },
    instances: [{ type: mongoose_1.Schema.Types.ObjectId, ref: 'Instance' }]
});
exports.User = mongoose_service_1.connection.model('User', exports.UserSchema);
//# sourceMappingURL=user.model.js.map