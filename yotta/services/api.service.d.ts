export declare class ApiService {
    private apiUrl;
    private token;
    constructor(apiUrl: string, token?: string);
    private _authRequestHandler<T>(url, method, body?);
    private _requestHandler<T>(url, method, body?, headers?);
    authenticate(key: string): Promise<void>;
    post(url: string, body: object): Promise<any>;
    put(url: string, body: object): Promise<any>;
    delete(url: string): Promise<any>;
    get(url: string): Promise<any>;
}
