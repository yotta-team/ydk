"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var request = require("request");
var ApiService = (function () {
    function ApiService(apiUrl, token) {
        this.apiUrl = apiUrl;
        this.token = token;
        if (apiUrl.endsWith('/'))
            this.apiUrl = apiUrl.substring(0, apiUrl.length - 1);
    }
    ApiService.prototype._authRequestHandler = function (url, method, body) {
        if (!this.token)
            return Promise.reject(new Error('Authentication it\'s required to use the api.'));
        return this._requestHandler(url, method, body, {
            'authorization': this.token
        });
    };
    ApiService.prototype._requestHandler = function (url, method, body, headers) {
        if (url.startsWith('/'))
            url = url.substring(1);
        url = this.apiUrl + "/" + url;
        return new Promise(function (resolve, reject) {
            var options = {
                url: url,
                body: body,
                json: true,
                method: method,
                headers: headers
            };
            var req = request(options, function (err, httpResponse, body) {
                if (err || httpResponse.statusCode !== 200)
                    reject(err || body);
                else
                    resolve(body);
            });
            req.on('error', function (err) {
                reject(err);
            });
        });
    };
    ApiService.prototype.authenticate = function (key) {
        var _this = this;
        return this._requestHandler("authenticate?key=" + key, 'GET')
            .then(function (res) { return _this.token = res.token; });
    };
    ApiService.prototype.post = function (url, body) {
        return this._authRequestHandler(url, 'POST', body);
    };
    ApiService.prototype.put = function (url, body) {
        return this._authRequestHandler(url, 'PUT', body);
    };
    ApiService.prototype.delete = function (url) {
        return this._authRequestHandler(url, 'DELETE');
    };
    ApiService.prototype.get = function (url) {
        return this._authRequestHandler(url, 'GET');
    };
    return ApiService;
}());
exports.ApiService = ApiService;
//# sourceMappingURL=api.service.js.map