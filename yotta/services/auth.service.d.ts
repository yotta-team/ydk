/// <reference types="express" />
import { Express } from 'express';
import { Logger } from './logger.service';
import { IInstance } from '../models/instance.model';
import { IUser } from '../models/user.model';
import { Query } from './query.service';
export declare class AuthService {
    query: Query<IUser>;
    logger: Logger;
    isValidPassword(user: IUser, password: string): boolean;
    constructor(app: Express, instance: IInstance, logger: Logger);
}
