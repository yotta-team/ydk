"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var md5 = require("md5");
var passport = require("passport");
var passport_local_1 = require("passport-local");
var expressSession = require("express-session");
var user_model_1 = require("../models/user.model");
var query_service_1 = require("./query.service");
var AuthService = (function () {
    function AuthService(app, instance, logger) {
        this.logger = logger;
        this.query = new query_service_1.Query(user_model_1.User);
        app.use(expressSession({ secret: instance.host + instance._id, cookie: { maxAge: 60000 }, resave: false, saveUninitialized: false }));
        app.use(passport.initialize());
        app.use(passport.session());
        passport.use(new passport_local_1.Strategy({
            usernameField: 'email',
            passwordField: 'password'
        }, function (email, password, done) {
            if (process.env.IsDev) {
                this.logger.log('IsDev login mocked!');
                email = 'michaelpereiradasilva@hotmail.com';
                password = '123456';
            }
            this.query.get({ email: email })
                .then(function (user) {
                if (!user || !this.isValidPassword(user, password)) {
                    this.logger.log("User " + email + " login failed at " + new Date());
                    return done(null, false, { message: 'Email e/ou senha inválidos.' });
                }
                if (user.status !== user_model_1.UserStatus.Active) {
                    this.logger.log("User " + email + " login failed at " + new Date());
                    return done(null, false, { message: 'Este usuário esta inativo.' });
                }
                this.logger.log("User " + email + " logged at " + new Date());
                done(null, user);
            });
        }));
        passport.serializeUser(function (user, done) {
            done(null, user._id);
        });
        passport.deserializeUser(function (id, done) {
            this.query.get({ _id: id })
                .then(function (user) {
                done(null, user);
            })
                .catch(function (err) {
                done(err, null);
            });
        });
    }
    AuthService.prototype.isValidPassword = function (user, password) {
        return md5(password) == user.password;
    };
    return AuthService;
}());
exports.AuthService = AuthService;
//# sourceMappingURL=auth.service.js.map