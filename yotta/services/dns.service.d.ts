import { IInstance } from '../models/instance.model';
export declare class DnsService {
    static PREFFIX: string;
    private options;
    constructor(options?: object);
    init(coreUrl: string): Promise<{}>;
    add(instance: IInstance, coreUrl: string): void;
    resolve(host: string): any;
}
