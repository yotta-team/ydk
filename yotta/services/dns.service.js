"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var instance_model_1 = require("../models/instance.model");
var DnsService = (function () {
    function DnsService(options) {
        this.options = options || {};
    }
    DnsService.prototype.init = function (coreUrl) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            instance_model_1.Instance.find(function (err, instances) {
                if (err)
                    return reject(err);
                for (var key in instances) {
                    _this.add(instances[key], coreUrl);
                }
                resolve();
            });
        });
    };
    DnsService.prototype.add = function (instance, coreUrl) {
        var server = require("../../instances/" + instance.host + "/server.js");
        server.start(instance, coreUrl);
        this.options[instance.host] = instance.protocol + "://" + instance.ip + ":" + instance.port;
        if (!instance.host.startsWith(DnsService.PREFFIX))
            this.options["" + DnsService.PREFFIX + instance.host] = instance.protocol + "://" + instance.ip + ":" + instance.port + "/yotta";
    };
    DnsService.prototype.resolve = function (host) {
        return this.options[host];
    };
    DnsService.PREFFIX = 'yotta';
    return DnsService;
}());
exports.DnsService = DnsService;
;
//# sourceMappingURL=dns.service.js.map