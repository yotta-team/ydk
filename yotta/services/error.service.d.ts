export declare class ErrorService {
    static default(res: any, e: Error): void;
    static notAuthorized(res: any): void;
}
