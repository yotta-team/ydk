"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ErrorService = (function () {
    function ErrorService() {
    }
    ErrorService.default = function (res, e) {
        console.error(e);
        res.status(500).json({ error: e.message });
    };
    ErrorService.notAuthorized = function (res) {
        res.status(401);
    };
    return ErrorService;
}());
exports.ErrorService = ErrorService;
//# sourceMappingURL=error.service.js.map