export declare class Logger {
    logPath: string;
    constructor(logPath: string);
    private _appendLine(message);
    log(...args: any[]): Promise<void[]>;
}
