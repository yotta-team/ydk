export declare class MailSettings {
    from: string;
    service: string;
    auth: {
        user: string;
        pass: string;
    };
}
export declare class Mailer {
    private _from;
    private _transporter;
    private _settings;
    constructor(settings: MailSettings);
    send(to: string, subject: string, html: string): Promise<void>;
    sendFile(to: string, subject: string, file: string, params?: any): Promise<void>;
}
