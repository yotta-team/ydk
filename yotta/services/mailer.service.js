"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var path = require("path");
var nodemailer_1 = require("nodemailer");
var fsx_util_1 = require("../utils/fsx.util");
var MailSettings = (function () {
    function MailSettings() {
    }
    return MailSettings;
}());
exports.MailSettings = MailSettings;
var Mailer = (function () {
    function Mailer(settings) {
        this._settings = settings;
        this._from = settings.from;
        // create reusable transporter object using the default SMTP transport
        this._transporter = nodemailer_1.createTransport({
            service: settings.service,
            auth: {
                user: settings.auth.user,
                pass: settings.auth.pass
            }
        });
    }
    Mailer.prototype.send = function (to, subject, html) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            var options = {
                from: _this._from,
                to: to,
                subject: subject,
                html: html
            };
            // send mail with defined transport object
            _this._transporter.sendMail(options, function (error, info) {
                if (error)
                    reject(error);
                resolve();
            });
        });
    };
    Mailer.prototype.sendFile = function (to, subject, file, params) {
        var _this = this;
        var source = path.resolve(__dirname + "/../emails/" + file);
        return fsx_util_1.fsx.readFile(source)
            .then(function (data) {
            var result = data;
            if (params) {
                for (var key in params)
                    result = result.replace(new RegExp("{{" + key + "}}", 'g'), params[key]);
            }
            return _this.send(to, subject, result);
        });
    };
    return Mailer;
}());
exports.Mailer = Mailer;
//# sourceMappingURL=mailer.service.js.map