"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var mongoose = require("mongoose");
var settings_service_1 = require("./settings.service");
mongoose.Promise = Promise;
var connectionString = process.env.MONGODB_CONNECTION || settings_service_1.Settings.get('connectionString');
if (!connectionString)
    throw new Error('It\'s required to set a connectionString to settings');
exports.connection = mongoose.createConnection(connectionString, {
    server: { socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 } },
    replset: { socketOptions: { keepAlive: 300000, connectTimeoutMS: 30000 } }
}); // connect to our database
exports.connection.on('connected', function () {
    console.log('Mongoose default connection open to ');
});
exports.connection.on('error', function (err) {
    console.log('Mongoose default connection error: ' + err);
});
exports.connection.on('disconnected', function () {
    console.log('Mongoose default connection disconnected');
});
exports.connection.on('open', function () {
    console.log('Mongoose default connection is open');
});
process.on('SIGINT', function () {
    exports.connection.close(function () {
        console.log('Mongoose default connection disconnected through app termination');
        process.exit(0);
    });
});
//# sourceMappingURL=mongoose.service.js.map