/// <reference types="mongoose" />
import { Document, Model } from 'mongoose';
export declare class Query<T extends Document> {
    private _model;
    private _constraint;
    constructor(model: Model<T>, constraint?: object);
    get(where?: object): Promise<T>;
    getAll(where?: any): Promise<T[]>;
    select<TResult>(projection: TResult, where?: any): Promise<TResult[]>;
    selectOne<TResult>(projection: TResult, where?: any): Promise<TResult>;
    max<TResult>(field: string, where?: any): Promise<TResult>;
}
