"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Query = (function () {
    function Query(model, constraint) {
        this._model = model;
        this._constraint = constraint || {};
    }
    Query.prototype.get = function (where) {
        var _this = this;
        Object.assign(where || {}, this._constraint);
        return new Promise(function (resolve, reject) {
            _this._model.findOne(where, function (err, model) {
                if (err)
                    return reject(err);
                else
                    resolve(model || undefined);
            });
        });
    };
    Query.prototype.getAll = function (where) {
        var _this = this;
        Object.assign(where || {}, this._constraint);
        return new Promise(function (resolve, reject) {
            _this._model.find(where, function (err, models) {
                if (err)
                    return reject(err);
                else
                    resolve(models || []);
            });
        });
    };
    Query.prototype.select = function (projection, where) {
        var _this = this;
        Object.assign(where || {}, this._constraint);
        return new Promise(function (resolve, reject) {
            _this._model.find(where, projection, function (err, models) {
                if (err)
                    return reject(err);
                else
                    resolve(models || []);
            });
        });
    };
    Query.prototype.selectOne = function (projection, where) {
        var _this = this;
        Object.assign(where || {}, this._constraint);
        return new Promise(function (resolve, reject) {
            _this._model.findOne(where, projection, function (err, model) {
                if (err)
                    return reject(err);
                else
                    resolve(model || undefined);
            });
        });
    };
    Query.prototype.max = function (field, where) {
        var _this = this;
        Object.assign(where || {}, this._constraint);
        return new Promise(function (resolve, reject) {
            _this._model.findOne(where).sort("-" + field) // give me the max
                .exec(function (err, result) {
                if (err)
                    return reject(err);
                else
                    resolve(result ? result[field] : undefined);
            });
        });
    };
    return Query;
}());
exports.Query = Query;
//# sourceMappingURL=query.service.js.map