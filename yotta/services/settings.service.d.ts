export declare class Settings {
    private static data;
    static get(key: string): any;
    static set(key: string, value: any): void;
    static readJSON(object: any): void;
}
