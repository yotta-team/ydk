"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Settings = (function () {
    function Settings() {
    }
    Settings.get = function (key) {
        return Settings.data.get(key);
    };
    Settings.set = function (key, value) {
        Settings.data.set(key, value);
    };
    Settings.readJSON = function (object) {
        for (var key in object) {
            Settings.set(key, object[key]);
        }
    };
    Settings.data = new Map();
    return Settings;
}());
exports.Settings = Settings;
//# sourceMappingURL=settings.service.js.map