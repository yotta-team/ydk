"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var child_process_1 = require("child_process");
var bash = (function () {
    function bash() {
    }
    bash.exec = function (command) {
        return new Promise(function (resolve, reject) {
            child_process_1.exec(command, function (err, stdout, stderr) {
                if (err)
                    reject(err);
                else
                    resolve();
            });
        });
    };
    return bash;
}());
exports.bash = bash;
//# sourceMappingURL=bash.util.js.map