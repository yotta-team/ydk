/// <reference types="mongoose" />
import { Document } from 'mongoose';
export declare class Commons {
    static removeId(obj: any): void;
    static cloneDoc<T extends Document>(doc: T): T;
    static cloneArray<T extends Document>(docs: T[]): T[];
    static urlToFileName(url: string): string;
    static stripAccents(text: string): string;
    static urlFrom(text: string): string;
    static anchorFrom(text: string): string;
}
