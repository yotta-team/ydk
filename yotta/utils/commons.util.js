"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Commons = (function () {
    function Commons() {
    }
    Commons.removeId = function (obj) {
        for (var key in obj) {
            if (key.startsWith("_"))
                delete obj[key];
            else if (typeof obj[key] == "object")
                Commons.removeId(obj[key]);
        }
    };
    Commons.cloneDoc = function (doc) {
        var obj = typeof (doc.toObject) === 'function' ? doc.toObject() : doc;
        Commons.removeId(obj);
        return obj;
    };
    Commons.cloneArray = function (docs) {
        var newDocs = docs.map(Commons.cloneDoc);
        return newDocs;
    };
    Commons.urlToFileName = function (url) {
        var parts = url.replace(/(\\|\/\/|\/$)/gi, '/').replace(/(^\/|\/$)/gi, '').split('/');
        var lastPart = parts[parts.length - 1];
        if (lastPart.indexOf('.html') === -1) {
            parts[parts.length - 1] += '/index.html';
        }
        return parts.join('/');
    };
    Commons.stripAccents = function (text) {
        var in_chrs = 'àáâãäçèéêëìíîïñòóôõöùúûüýÿÀÁÂÃÄÇÈÉÊËÌÍÎÏÑÒÓÔÕÖÙÚÛÜÝ', out_chrs = 'aaaaaceeeeiiiinooooouuuuyyAAAAACEEEEIIIINOOOOOUUUUY', chars_rgx = new RegExp('[' + in_chrs + ']', 'g'), transl = {}, i, lookup = function (m) { return transl[m] || m; };
        for (i = 0; i < in_chrs.length; i++) {
            transl[in_chrs[i]] = out_chrs[i];
        }
        return text.replace(chars_rgx, lookup);
    };
    Commons.urlFrom = function (text) {
        return Commons.stripAccents(text.toLowerCase())
            .replace(/\s/g, '-')
            .replace(/-+/g, '-')
            .replace(/\\/g, '/')
            .replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
    };
    Commons.anchorFrom = function (text) {
        return '#' + Commons.stripAccents(text.toLowerCase())
            .replace(/(\s|\/|\.)/g, '-')
            .replace(/-+/g, '-')
            .replace(/[&\/\\#,+()$~%.'":*?<>{}]/g, '');
    };
    return Commons;
}());
exports.Commons = Commons;
//# sourceMappingURL=commons.util.js.map