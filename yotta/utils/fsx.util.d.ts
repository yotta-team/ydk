/// <reference types="node" />
export declare class fsx {
    static exists(dir_path: string | Buffer): Promise<boolean>;
    static isEmpty(dir_path: string | Buffer): Promise<boolean>;
    static isdir(entry_path: string | Buffer): Promise<boolean>;
    static extractZip(source: string, target: string): Promise<{}>;
    static readdir(dir_path: string | Buffer): Promise<string[]>;
    static mkdir(dir_path: string | Buffer): Promise<void>;
    static rmdir(dir_path: string | Buffer): Promise<void>;
    static removeFile(entry_path: string | Buffer): Promise<void>;
    static writeFile(entry_path: string, content: any, options?: string | any): Promise<void>;
    static readFile(entry_path: string, contentType?: string): Promise<string>;
    static appendFile(entry_path: string, content: any): Promise<void>;
    static ncp(dir_path: string, target_path: string): Promise<void>;
    static copy(source: string | Buffer, target: string): Promise<any>;
    static move(entry_path: string | Buffer, target_path: string): Promise<void>;
    static rename(entry_path: string, target_path: string): Promise<void>;
    /**
     * Remove directory recursively
     * @param {string} dir_path
     * @see http://stackoverflow.com/a/42505874/3027390
     */
    static rimraf(dir_path: string): Promise<void>;
}
