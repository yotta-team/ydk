"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var fs = require("fs");
var path = require("path");
var ncp_1 = require("ncp");
var unzip = require('unzip');
var fsx = (function () {
    function fsx() {
    }
    fsx.exists = function (dir_path) {
        return new Promise(function (resolve, reject) {
            fs.exists(dir_path, function (exists) {
                resolve(exists);
            });
        });
    };
    fsx.isEmpty = function (dir_path) {
        return fsx.readdir(dir_path).then(function (files) { return files.length === 0; });
    };
    fsx.isdir = function (entry_path) {
        return new Promise(function (resolve, reject) {
            fs.lstat(entry_path, function (err, stats) {
                if (err)
                    reject(err);
                else
                    resolve(stats.isDirectory());
            });
        });
    };
    fsx.extractZip = function (source, target) {
        return new Promise(function (resolve, reject) {
            fs.createReadStream(source)
                .pipe(unzip.Extract({ path: target }))
                .on('close', function (err) {
                if (err)
                    reject(err);
                else
                    resolve();
            });
        });
    };
    fsx.readdir = function (dir_path) {
        return new Promise(function (resolve, reject) {
            fs.readdir(dir_path, function (err, files) {
                if (err)
                    reject(err);
                else
                    resolve(files);
            });
        });
    };
    fsx.mkdir = function (dir_path) {
        return new Promise(function (resolve, reject) {
            fs.mkdir(dir_path, function (err) {
                if (err)
                    reject(err);
                else
                    resolve();
            });
        });
    };
    fsx.rmdir = function (dir_path) {
        return new Promise(function (resolve, reject) {
            fs.rmdir(dir_path, function (err) {
                if (err)
                    reject(err);
                else
                    resolve();
            });
        });
    };
    fsx.removeFile = function (entry_path) {
        return new Promise(function (resolve, reject) {
            fs.unlink(entry_path, function (err) {
                if (err)
                    reject(err);
                else
                    resolve();
            });
        });
    };
    fsx.writeFile = function (entry_path, content, options) {
        return new Promise(function (resolve, reject) {
            fs.writeFile(entry_path, content, options, function (err) {
                if (err)
                    reject(err);
                else
                    resolve();
            });
        });
    };
    fsx.readFile = function (entry_path, contentType) {
        return new Promise(function (resolve, reject) {
            fs.readFile(entry_path, contentType || 'utf8', function (err, result) {
                if (err)
                    reject(err);
                else
                    resolve(result);
            });
        });
    };
    fsx.appendFile = function (entry_path, content) {
        return new Promise(function (resolve, reject) {
            fsx.exists(entry_path)
                .then(function (exists) {
                if (exists)
                    return fsx.appendFile(entry_path, content);
                else
                    return fsx.writeFile(entry_path, content);
            });
        });
    };
    fsx.ncp = function (dir_path, target_path) {
        return new Promise(function (resolve, reject) {
            ncp_1.ncp(dir_path, target_path, function (err) {
                if (err)
                    return reject(err);
                else
                    resolve();
            });
        });
    };
    fsx.copy = function (source, target) {
        return new Promise(function (resolve, reject) {
            var dirname = path.dirname(target);
            return fsx.exists(dirname)
                .then(function (exists) {
                if (!exists)
                    return fsx.mkdir(dirname);
            })
                .then(function () {
                var rd = fs.createReadStream(source);
                rd.on('error', rejectCleanup);
                var wr = fs.createWriteStream(target);
                wr.on('error', rejectCleanup);
                function rejectCleanup(err) {
                    rd.destroy();
                    wr.end();
                    reject(err);
                }
                wr.on('finish', resolve);
                rd.pipe(wr);
            })
                .catch(reject);
        });
    };
    fsx.move = function (entry_path, target_path) {
        return fsx.copy(entry_path, target_path)
            .then(function () { return fs.unlinkSync(entry_path); });
    };
    fsx.rename = function (entry_path, target_path) {
        return new Promise(function (resolve, reject) {
            fs.rename(entry_path, target_path, function (err) {
                if (err)
                    reject(err);
                else
                    resolve();
            });
        });
    };
    /**
     * Remove directory recursively
     * @param {string} dir_path
     * @see http://stackoverflow.com/a/42505874/3027390
     */
    fsx.rimraf = function (dir_path) {
        function remove(dir_path) {
            return fsx.exists(dir_path)
                .then(function (exists) { return exists ? fsx.readdir(dir_path) : []; })
                .then(function (files) {
                var promises = [];
                files.forEach(function (entry) {
                    var entry_path = path.join(dir_path, entry);
                    promises.push(fsx.isdir(entry_path)
                        .then(function (isdir) { return isdir ? remove(entry_path) : fsx.removeFile(entry_path).then(function () { return ''; }); }));
                });
                return Promise.all(promises);
            })
                .then(function (dirs) { return Promise.all(dirs.filter(function (d) { return d.trim().length > 0; }).map(fsx.rmdir)); })
                .then(function () { return dir_path; });
        }
        var hasStar = dir_path.endsWith('*');
        dir_path = hasStar ? dir_path.substr(0, dir_path.length - 1) : dir_path;
        return remove(dir_path)
            .then(function () {
            if (!hasStar)
                return fsx.rmdir(dir_path);
        });
    };
    return fsx;
}());
exports.fsx = fsx;
//# sourceMappingURL=fsx.util.js.map