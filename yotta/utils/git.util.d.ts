import { IRepository } from '../models/repository.model';
export declare class git {
    static clone(repoName: string, path: string): Promise<void>;
    static cloneRepository(repository: IRepository, path: string): Promise<void>;
    static checkout(path: string, files?: string): Promise<void>;
    static commit(message: string, path: string, files?: string): Promise<void>;
    static push(path: string, remote: string, branch?: string): Promise<void>;
    static pull(path: string, remote: string, branch?: string): Promise<void>;
}
