export declare class Listener {
    private _subscribers;
    constructor();
    subscribe(callback: (params?: any[]) => any): void;
    notifyAll(params?: any[]): void;
}
