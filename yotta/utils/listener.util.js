"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var Listener = (function () {
    function Listener() {
        this._subscribers = [];
    }
    Listener.prototype.subscribe = function (callback) {
        this._subscribers.push(callback);
    };
    Listener.prototype.notifyAll = function (params) {
        if (this._subscribers.length) {
            for (var i = 0; i < this._subscribers.length; i++) {
                this._subscribers[i].apply(this, params || []);
            }
        }
    };
    return Listener;
}());
exports.Listener = Listener;
//# sourceMappingURL=listener.util.js.map