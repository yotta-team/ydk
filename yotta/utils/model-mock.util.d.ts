/// <reference types="mongoose" />
import { Document, Model } from 'mongoose';
export declare class ModelMock<T extends Document> {
    private model;
    constructor(model: Model<T>);
    private _createOrUpdateOneItem(keys, data);
    createOrUpdate(keys: string | string[], data: object | object[]): Promise<T[]>;
    createOrUpdateOne(where: object, data: object): Promise<T>;
}
