"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var ModelMock = (function () {
    function ModelMock(model) {
        this.model = model;
    }
    ModelMock.prototype._createOrUpdateOneItem = function (keys, data) {
        var where = {};
        keys.forEach(function (key) { return where[key] = data[key]; });
        return this.createOrUpdateOne(where, data);
    };
    ModelMock.prototype.createOrUpdate = function (keys, data) {
        var _this = this;
        if (!keys)
            throw new Error("It's required to pass a key to createOrUpdate method");
        if (!Array.isArray(keys))
            keys = [keys];
        if (Array.isArray(data))
            return Promise.all(data.map(function (d) { return _this._createOrUpdateOneItem(keys, d); }));
        else
            return this._createOrUpdateOneItem(keys, data).then(function (model) { return [model]; });
    };
    ModelMock.prototype.createOrUpdateOne = function (where, data) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.model.findOneAndUpdate(where, data, function (err, model) {
                if (err)
                    return reject(err);
                if (!model) {
                    model = new _this.model(data);
                }
                model.save(function (err) {
                    if (err)
                        return reject(err);
                    resolve(model);
                });
            });
        });
    };
    return ModelMock;
}());
exports.ModelMock = ModelMock;
;
//# sourceMappingURL=model-mock.util.js.map