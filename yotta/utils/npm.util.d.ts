import { IRepository } from '../models/repository.model';
export declare class npm {
    static install(name: string, save?: boolean): Promise<void>;
    static installRepository(repository: IRepository, save?: boolean): Promise<void>;
    static uninstall(name: string, save?: boolean): Promise<void>;
    static uninstallRepository(repository: IRepository, save?: boolean): Promise<void>;
    static update(name?: string, save?: boolean): Promise<void>;
    static run(scriptName: string): Promise<void>;
}
