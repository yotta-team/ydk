"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var bash_util_1 = require("./bash.util");
var npm = (function () {
    function npm() {
    }
    npm.install = function (name, save) {
        if (save === void 0) { save = false; }
        var args = save ? '--save' : '';
        return bash_util_1.bash.exec("npm install " + name + " " + args);
    };
    npm.installRepository = function (repository, save) {
        if (save === void 0) { save = false; }
        return this.install("" + repository.url, save);
    };
    npm.uninstall = function (name, save) {
        if (save === void 0) { save = false; }
        var args = save ? '--save' : '';
        return bash_util_1.bash.exec("npm uninstall " + name + " " + args);
    };
    npm.uninstallRepository = function (repository, save) {
        if (save === void 0) { save = false; }
        return this.uninstall("" + repository.name, save);
    };
    npm.update = function (name, save) {
        if (name === void 0) { name = ''; }
        if (save === void 0) { save = false; }
        var args = save ? '--save' : '';
        return bash_util_1.bash.exec("npm update " + name + " " + args);
    };
    npm.run = function (scriptName) {
        return bash_util_1.bash.exec("npm run " + scriptName);
    };
    return npm;
}());
exports.npm = npm;
//# sourceMappingURL=npm.util.js.map