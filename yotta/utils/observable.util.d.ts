export declare class Observable {
    private _events;
    constructor();
    on(eventName: string, callback: (params?: any[]) => any): void;
    trigger(eventName: string, params?: any[]): void;
}
