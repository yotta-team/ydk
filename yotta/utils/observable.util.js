"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
var listener_util_1 = require("./listener.util");
var Observable = (function () {
    function Observable() {
        this._events = new Map();
    }
    Observable.prototype.on = function (eventName, callback) {
        if (!this._events.has(eventName))
            this._events.set(eventName, new listener_util_1.Listener());
        this._events.get(eventName).subscribe(callback);
    };
    Observable.prototype.trigger = function (eventName, params) {
        if (this._events.has(eventName)) {
            this._events.get(eventName).notifyAll(params || []);
        }
    };
    return Observable;
}());
exports.Observable = Observable;
//# sourceMappingURL=observable.util.js.map